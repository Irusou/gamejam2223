#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenu;

    public void StartGame()
    {
        SceneManager.LoadScene("level1");
        Time.timeScale = 1.0f;
    }

    public void Menu() => SceneManager.LoadScene("level1");

    private void PauseGame()
    {
        Time.timeScale = 0.0f;
        pauseMenu.SetActive(true);
    }

    public void ResumeGame()
    {
        Time.timeScale = 1.0f;
        pauseMenu.SetActive(false);
    }
    
    public void QuitGame()
    {
        Application.Quit();

    #if UNITY_EDITOR
            EditorApplication.ExitPlaymode();
    #endif
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
        }
    }
}
